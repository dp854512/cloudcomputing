from common import up_map as map_func, up_reduce as reduce_func, shuffle
import multiprocessing as mp
map_in = []
if __name__ == '__main__':

	with open('AComp_Passenger_data_no_error.csv', encoding="utf8") as f:
		map_in = f.read().splitlines()
		with mp.Pool(processes=mp.cpu_count()) as pool:
			map_out = pool.map(map_func, map_in, chunksize=int(len(map_in)/mp.cpu_count()))
			reduce_in = shuffle(map_out)
			reduce_out = pool.map(reduce_func, reduce_in.items(), chunksize=int(len(reduce_in.keys())/mp.cpu_count()))
			#print(reduce_out)
			reduce_out.sort(key=lambda a: a[1],reverse=True)
			print('Trips taken by passengers:')
			for j,k in reduce_out:
					print(j,k)
