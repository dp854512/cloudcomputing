#shuffle function
def shuffle(mapper_out):
	data = {}
	mapper_out = list(filter(None, mapper_out))
	for k, v in mapper_out:
		if k not in data:
			data[k] = [v]
		else:
			data[k].append(v)
	return data

#map function
def up_map(x):
	return (x.split(',')[0],1)
	
#reduce function
def up_reduce(z):
	k, v = z
	return (k, sum(v))